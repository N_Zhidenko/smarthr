/**
 * Cube - Bootstrap Admin Theme
 * Copyright 2014 Phoonio
 */

var app = angular.module('cubeWebApp', [
	'ngRoute',
	'angular-loading-bar',
	'ngAnimate',
	'easypiechart'
]);

app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
	cfpLoadingBarProvider.includeBar = true;
	cfpLoadingBarProvider.includeSpinner = true;
	cfpLoadingBarProvider.latencyThreshold = 100;
}]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
	$routeProvider
		.when("/", {
			redirectTo:'/dasboard'
		})
		.when("/dasboard", {
			templateUrl: "/template/angularjs/views/dashboard.html", 
			controller: "mainCtrl", 
			title: 'Dashboard'
		})
		.when("/tables/simple", {
			templateUrl: "/template/angularjs/views/tables.html", 
			controller: "mainCtrl", 
			title: 'Tables'
		})
		.when("/tables/tables-advanced", {
			templateUrl: "/template/angularjs/views/tables-advanced.html", 
			controller: "mainCtrl", 
			title: 'Advanced tables'
		})
		.when("/tables/users", {
			templateUrl: "/template/angularjs/views/users.html", 
			controller: "mainCtrl", 
			title: 'Users'
		})
		.when("/tables/footables", {
			templateUrl: "/template/angularjs/views/tables-footables.html", 
			controller: "mainCtrl", 
			title: 'FooTables'
		})

		.when("/graphs/graphs-xcharts", {
			templateUrl: "/template/angularjs/views/graphs-xcharts.html", 
			controller: "mainCtrl", 
			title: 'XCharts'
		})
		.when("/graphs/graphs-morris", {
			templateUrl: "/template/angularjs/views/graphs-morris.html", 
			controller: "mainCtrl", 
			title: 'Morris charts'
		})
		.when("/graphs/graphs-flot", {
			templateUrl: "/template/angularjs/views/graphs-flot.html", 
			controller: "mainCtrl", 
			title: 'Flot charts'
		})
		.when("/graphs/graphs-dygraphs", {
			templateUrl: "/template/angularjs/views/graphs-dygraphs.html", 
			controller: "mainCtrl", 
			title: 'Dygraphs'
		})

		.when("/email/email-compose", {
			templateUrl: "/template/angularjs/views/email-compose.html", 
			controller: "emailCtrl", 
			title: 'Email compose'
		})
		.when("/email/email-inbox", {
			templateUrl: "/template/angularjs/views/email-inbox.html", 
			controller: "emailCtrl", 
			title: 'Email inbox'
		})
		.when("/email/email-detail", {
			templateUrl: "/template/angularjs/views/email-detail.html", 
			controller: "emailCtrl", 
			title: 'Email detail'
		})

		.when("/widgets", {
			templateUrl: "/template/angularjs/views/widgets.html", 
			controller: "mainCtrl", 
			title: 'Widgets'
		})

		.when("/pages/user-profile", {
			templateUrl: "/template/angularjs/views/user-profile.html", 
			controller: "mainCtrl", 
			title: 'User profile'
		})
		.when("/pages/calendar", {
			templateUrl: "/template/angularjs/views/calendar.html", 
			controller: "mainCtrl", 
			title: 'Calendar'
		})
		.when("/pages/timeline", {
			templateUrl: "/template/angularjs/views/timeline.html", 
			controller: "mainCtrl", 
			title: 'Timeline'
		})
		.when("/pages/timeline-grid", {
			templateUrl: "/template/angularjs/views/timeline-grid.html", 
			controller: "mainCtrl", 
			title: 'Timeline grid'
		})
		.when("/pages/team-members", {
			templateUrl: "/template/angularjs/views/team-members.html", 
			controller: "mainCtrl", 
			title: 'Team members'
		})
		.when("/pages/search", {
			templateUrl: "/template/angularjs/views/search.html", 
			controller: "mainCtrl", 
			title: 'Search results'
		})
		.when("/pages/projects", {
			templateUrl: "/template/angularjs/views/projects.html", 
			controller: "mainCtrl", 
			title: 'Projects'
		})
		.when("/pages/pricing", {
			templateUrl: "/template/angularjs/views/pricing.html", 
			controller: "mainCtrl", 
			title: 'Pricing'
		})
		.when("/pages/invoice", {
			templateUrl: "/template/angularjs/views/invoice.html", 
			controller: "mainCtrl", 
			title: 'Invoice'
		})
		.when("/pages/intro", {
			templateUrl: "/template/angularjs/views/intro.html", 
			controller: "mainCtrl", 
			title: 'Tour layout'
		})
		.when("/pages/gallery", {
			templateUrl: "/template/angularjs/views/gallery.html", 
			controller: "mainCtrl", 
			title: 'Gallery'
		})
		.when("/pages/gallery-v2", {
			templateUrl: "/template/angularjs/views/gallery-v2.html", 
			controller: "mainCtrl", 
			title: 'Gallery v2'
		})

		.when("/forms/x-editable", {
			templateUrl: "/template/angularjs/views/x-editable.html", 
			controller: "mainCtrl", 
			title: 'X-Editable'
		})
		.when("/forms/form-elements", {
			templateUrl: "/template/angularjs/views/form-elements.html", 
			controller: "mainCtrl", 
			title: 'Form elements'
		})
		.when("/forms/form-ckeditor", {
			templateUrl: "/template/angularjs/views/form-ckeditor.html", 
			controller: "mainCtrl", 
			title: 'Ckeditor'
		})
		.when("/forms/form-wysiwyg", {
			templateUrl: "/template/angularjs/views/form-wysiwyg.html", 
			controller: "mainCtrl", 
			title: 'Wysiwyg'
		})
		.when("/forms/form-wizard", {
			templateUrl: "/template/angularjs/views/form-wizard.html", 
			controller: "mainCtrl", 
			title: 'Wizard'
		})
		.when("/forms/form-wizard-popup", {
			templateUrl: "/template/angularjs/views/form-wizard-popup.html", 
			controller: "mainCtrl", 
			title: 'Wizard popup'
		})
		.when("/forms/form-dropzone", {
			templateUrl: "/template/angularjs/views/form-dropzone.html", 
			controller: "mainCtrl", 
			title: 'Dropzone'
		})
		.when("/forms/form-summernote", {
			templateUrl: "/template/angularjs/views/form-summernote.html", 
			controller: "mainCtrl", 
			title: 'Wysiwyg Summernote'
		})

		.when("/ui-kit/ui-elements", {
			templateUrl: "/template/angularjs/views/ui-elements.html", 
			controller: "mainCtrl", 
			title: 'UI elements'
		})
		.when("/ui-kit/ui-nestable", {
			templateUrl: "/template/angularjs/views/ui-nestable.html", 
			controller: "mainCtrl", 
			title: 'UI nestable'
		})
		.when("/ui-kit/video", {
			templateUrl: "/template/angularjs/views/video.html", 
			controller: "mainCtrl", 
			title: 'Video'
		})
		.when("/ui-kit/typography", {
			templateUrl: "/template/angularjs/views/typography.html", 
			controller: "mainCtrl", 
			title: 'Typography'
		})
		.when("/ui-kit/notifications", {
			templateUrl: "/template/angularjs/views/notifications.html", 
			controller: "mainCtrl", 
			title: 'Notifications and Alerts'
		})
		.when("/ui-kit/modals", {
			templateUrl: "/template/angularjs/views/modals.html", 
			controller: "mainCtrl", 
			title: 'Modals'
		})
		.when("/ui-kit/icons/icons-awesome", {
			templateUrl: "/template/angularjs/views/icons-awesome.html", 
			controller: "mainCtrl", 
			title: 'Awesome icons'
		})
		.when("/ui-kit/icons/icons-halflings", {
			templateUrl: "/template/angularjs/views/icons-halflings.html", 
			controller: "mainCtrl", 
			title: 'Halflings icons'
		})

		.when("/google-maps", {
			templateUrl: "/template/angularjs/views/maps.html", 
			controller: "mainCtrl", 
			title: 'Google maps'
		})

		.when("/extra/faq", {
			templateUrl: "/template/angularjs/views/faq.html", 
			controller: "mainCtrl", 
			title: 'FAQ'
		})
		.when("/extra/extra-grid", {
			templateUrl: "/template/angularjs/views/extra-grid.html", 
			controller: "mainCtrl", 
			title: 'Extra grid'
		})
		.when("/extra/email-templates", {
			templateUrl: "/template/angularjs/views/emails.html", 
			controller: "mainCtrl", 
			title: 'Email templates'
		})

		.when("/error-404-v2", {
			templateUrl: "/template/angularjs/views/error-404-v2.html", 
			controller: "mainCtrl",
			title: 'Error 404'
		})
		.when("/error-404", {
			templateUrl: "/template/angularjs/views/error-404-v2.html", 
			controller: "mainCtrl",
			title: 'Error 404'
		})
		.otherwise({
			redirectTo:'/error-404'
		});
}]);

app.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
    });
}]);